extends MarginContainer

signal toggle_state

func toggle_state(state):
    if state == true:
        $TextureRect.texture = load("res://assets/Hearts/heart_basic.png")
    else:
        $TextureRect.texture = load("res://assets/Hearts/heart_outline_empty.png")
