extends Control

signal end_game


func _input(event):
    if(visible and event.is_action_pressed("ui_accept")):
        emit_signal("end_game")
