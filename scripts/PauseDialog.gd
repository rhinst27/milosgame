extends WindowDialog

signal unpause

func _ready():
	set_process_input(true)


func _input(event):  
	if Input.is_action_just_pressed("ui_cancel"):  
		emit_signal("unpause")
