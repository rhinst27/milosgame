extends Control

signal finish_stage

func _input(event):
	if(visible and event.is_action_pressed("ui_accept")):
		emit_signal("finish_stage")

