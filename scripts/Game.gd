extends Node

onready var player_vars = get_node("/root/GlobalSettings")


var missile_scene = preload("res://scenes/Arrow.tscn") as PackedScene
var bomb_scene = preload("res://scenes/Bomb.tscn") as PackedScene

func _ready():        
	next_level()
	$HUDCanvas/HUD.connect("mute_music", self, "_mute_music")
	$HUDCanvas/HUD.connect("unmute_music", self, "_unmute_music")   

func _mute_music():
	$BackgroundMusic.stop()

func _unmute_music():
	$BackgroundMusic.play()	

func next_level():        
	#load current level scene
	var next_level = $Levels.get_next_level()
	if next_level == "":
		return
		
	if player_vars.current_level != null:             
		player_vars.current_level.get_node("FinishLine").disconnect("finish_line_crossed", self, "_on_finish_line_crossed")
		player_vars.current_level.get_node("FinishLine").queue_free()
		player_vars.current_level.queue_free()
	
	player_vars.current_level = load(next_level).instance()
		
	add_child(player_vars.current_level)
	
	# set player to spawn point    
	$Player.set_position(player_vars.current_level.get_node("SpawnPoint").get_global_position())
	
	#connect level finish line to handler code    
	player_vars.current_level.get_node("FinishLine").connect("finish_line_crossed", self, "_on_finish_line_crossed")	
	
	#regain health
	player_vars.total_hearts = player_vars.DIFFICULTY_SETTINGS[player_vars.difficulty_mode]["starting_health"]
	player_vars.health = player_vars.total_hearts   
	
	#initialize HUD
	$HUDCanvas/HUD.set_health(player_vars.health, player_vars.total_hearts)    
	$HUDCanvas/HUD.set_level(player_vars.level)
	$HUDCanvas/HUD.set_stars(player_vars.stars)
		
	print_debug("UNPAUSING")
	get_tree().paused = false
	player_vars.started = true
	
func _show_game_over():
	print_debug("GAME OVER")    
	get_tree().paused = true
	$Dialogs/GameOverDialog.show()
	
func _input(event):
	if event.is_action_pressed("ui_focus_next"):
		finish_stage()

func end_game():
	print("GAME END")    
	get_tree().change_scene("res://scenes/MainMenu.tscn")

func finish_stage():
	print_debug("Calling finish_stage")
	$Dialogs/StageCompleteDialog.hide()
	player_vars.stars += 10
	player_vars.level += 1    
	$BackgroundMusic.seek(0)  
	_goto_shop()
	
func _goto_shop():
	print("Showing Shop")
	$Dialogs/Shop.show()

   
func _pause_game():    
	print("PAUSING GAME")
	get_tree().paused = true 
	$Dialogs/PauseDialog.show()

func _unpause_game():        
	$Dialogs/PauseDialog.hide()
	get_tree().paused = false    
	
	
func _place_finish_line():
	$Ground.get_used_rect().end.y
	var pos = $Ground.map_to_world($Ground.getFinishLinePosition())        
	pos.y -= 200
	$FinishLine.position = pos

func _on_MissileTimer_timeout():   
	if player_vars.started and not player_vars.game_over and not player_vars.stage_complete and randf() < player_vars.DIFFICULTY_SETTINGS[player_vars.difficulty_mode]["missile_frequency"]:
		var missile = missile_scene.instance()    
		var viewport_end: Vector2 = get_viewport().get_visible_rect().end
		missile.position.x = $Player.global_position.x + viewport_end.x
		missile.position.y = $Player.global_position.y + (randi() % 200) - 100    
		missile.connect("hit", self, "_on_missile_hit")
		$ArrowCanvas.add_child(missile)    
		
		
func explode_bomb():
	var bomb_damage: int = player_vars.DIFFICULTY_SETTINGS[player_vars.difficulty_mode]["bomb_damage"]
	_take_damage(bomb_damage)

func _on_BombTimer_timeout():
	
	if player_vars.started and not player_vars.game_over and not player_vars.stage_complete and randf() < player_vars.DIFFICULTY_SETTINGS[player_vars.difficulty_mode]["bomb_frequency"]:        
		var bomb = bomb_scene.instance()            
		var viewport_start: Vector2 = get_viewport().get_visible_rect().position 
		var viewport_size: Vector2 = get_viewport().size             
		bomb.position.x = $Player.position.x - (viewport_size.x / 2) + (viewport_size.x * randf())
		bomb.position.y = viewport_start.y - (viewport_size.y / 2) 
		bomb.connect("hit", self, "explode_bomb")
		$ArrowCanvas.add_child(bomb)    

func _take_damage(damage: int):
	player_vars.health = max(player_vars.health - damage, 0)
	$Player.get_hurt() 
	$HUDCanvas/HUD.set_health(player_vars.health, player_vars.total_hearts)
	if player_vars.health == 0:
		_show_game_over()


func _on_missile_hit(collision_normal):    
	var missile_damage: int = player_vars.DIFFICULTY_SETTINGS[player_vars.difficulty_mode]["missile_damage"]	
	_take_damage(missile_damage)
	
	
func _on_finish_line_crossed(body):    
	if(body.name == 'Player'):
		player_vars.current_level.get_node("FinishLine").set_deferred("monitoring", false)
		print_debug("Player crossed finish line")
		get_tree().paused = true 
		$Dialogs/StageCompleteDialog.show()


func back_to_menu():
	get_tree().change_scene("res://scenes/MainMenu.tscn")


func _on_PauseDialog_quit_game():	
	get_tree().change_scene("res://scenes/MainMenu.tscn")
	player_vars.current_level = null


func _on_exit_shop():
	$Dialogs/Shop.hide()
	next_level()


func _on_finish_stage():
	finish_stage()
