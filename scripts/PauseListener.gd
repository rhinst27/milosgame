extends Node

signal unpause
signal pause

func _ready():
    set_process_input(true)

func _input(_event): 
    if Input.is_action_just_pressed("ui_cancel"):        
        if not get_tree().paused:            
            emit_signal("pause")                
        else:
            emit_signal("unpause")

