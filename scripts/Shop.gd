extends Node2D


signal exit_shop

func _on_Shop_show():
    print_debug("show shop and music")
    $BackgroundMusic.seek(0)
    $BackgroundMusic.play()


func _on_exit_pressed():
    emit_signal("exit_shop")


func _on_Shop_hide():
    print_debug("hide shop and music")
    $BackgroundMusic.stop()


func on_visibility_changed():
    print_debug("Shop visibility changed")
    if visible:
        _on_Shop_show()
    else:
        _on_Shop_hide()
