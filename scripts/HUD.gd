extends MarginContainer

signal mute_music
signal unmute_music

func _ready():
    $HBoxContainer/GameInfoBar/GameOptions/MusicToggleButton.connect("toggled", self, "_toggle_music")
    
func _toggle_music(toggled):
    if toggled:
        emit_signal("mute_music")
    else:
        emit_signal("unmute_music")
    

func set_health(cur_health: int, max_health):    
    $HBoxContainer/Bars/LifeBar.set_health(cur_health, max_health)

func set_level(level: int):
    $HBoxContainer/GameInfoBar/LevelLabel.text = "Level " + str(level)
    
func set_stars(stars: int):
    $HBoxContainer/Bars/StarBar/StarCount.text = str(stars)
