extends Node


const DEFAULT_MODE = 0 # Easy

var DIFFICULTY_SETTINGS = [	
	{
		"name": "EASY",
		"starting_health": 8,
		"missile_damage": 1,
		"bomb_damage": 2,
		"missile_frequency": 0.6,
		"bomb_frequency": 0.4
	},	
	{
		"name": "MEDIUM",
		"starting_health": 6,
		"missile_damage": 2,
		"bomb_damage": 3,
		"missile_frequency": 0.8,
		"bomb_frequency": 0.6
	},		
	{
		"name": "HARD",
		"starting_health": 4,
		"missile_damage": 3,
		"bomb_damage": 4,
		"missile_frequency": 0.9,
		"bomb_frequency": 0.75
	}	
]


var difficulty_mode = DEFAULT_MODE
var health = 0
var total_hearts = 0
var level = 1
var stars = 0
var started = false
var game_over = false
var stage_complete = false
var current_level = null
