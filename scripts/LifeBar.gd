extends HBoxContainer

var max_hearts = 3
var cur_hearts = 3

var heart_scene = load("res://scenes/Heart.tscn") as PackedScene


func set_health(cur_health: int, max_health: int):
    cur_hearts = cur_health
    max_hearts = max_health
    _redraw()
    
   
func _redraw():
    for heart in get_children():
        heart.queue_free()
        
    for i in range(max_hearts):
        var heart = heart_scene.instance()
        if i >= cur_hearts:                      
            heart.toggle_state(false)            
        add_child(heart)    
