extends TileMap


func _ready():
    randomize()


func getFinishLinePosition():    
    var x = null
    var y = null
    for cell_pos in get_used_cells():        
        if x == null or cell_pos.x > x:            
            x = cell_pos.x            
            y = cell_pos.y
        elif cell_pos.x == x and cell_pos.y < y:            
            y = cell_pos.y
    if (x == null) or (y == null):
        return null
    return Vector2(x,y)    
    
    

func generate_map():
    clear()
    var consecutive = 0
    var y = 5
    var max_y = y
    var coords = []
    for x in range(100):
        consecutive += 1
        set_cell(x, y, 1)
        coords.append(y)
        if y > max_y:
            max_y = y
        if ((consecutive > 2) and (randi() % 5) > 2):
            var delta = ((randi() % 3) - 1)
            if delta != 0:
                consecutive = 0
                y += delta
    for x in range(100):
        for y in range(coords[x]+1, max_y+10):
            set_cell(x, y, 0)
            
