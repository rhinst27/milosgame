extends RigidBody2D

signal hit


func _on_VisibilityNotifier2D_viewport_exited(_viewport):    
	queue_free()


func _on_Bomb_body_entered(body):    
	explode()
	if body.name == "Player":
		emit_signal("hit")
	
func explode():    
	$BombCollision.disabled = true
	$ExplosionCollision.disabled = false
	$Explosion.emitting = true
	$Sprite.visible = false
	yield(get_tree().create_timer(0.5), "timeout")
	queue_free()
