extends Area2D

signal finish_line_crossed

func _on_FinishLine_body_entered(body):        
	emit_signal("finish_line_crossed", body)
	
	
