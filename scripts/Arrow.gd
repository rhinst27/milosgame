extends KinematicBody2D

signal hit

const SPEED = -500

var velocity: Vector2 = Vector2()
var collided: bool = false
var collided_body
var collision_transform: Transform2D = Transform2D()

func _ready():
	set_physics_process(true) 

func _init():
	# gets called each time a new arrow is instantiated
	pass

func _physics_process(delta):
	if collided:
		_apply_collision_transform()
	else:
		_apply_normal_physics(delta)
		
func _apply_normal_physics(delta):
	var collision_info: KinematicCollision2D = move_and_collide(Vector2(SPEED * delta, 0))
	if collision_info:      
		# let the game know the arrow hit the player       
		emit_signal("hit", collision_info.normal.normalized())
		
		# don't process further physics/collissions for this arrow
		collided = true
		$CollisionShape2D.disabled = true
		
		# let arrow "sink in" a little more
		position += Vector2(-20, 0)
		
		
		# make the arrow "stick" to the player
		# we do this by translating the arrow's world coordinates to 
		# local coordinates within the player's scene
		collided_body = collision_info.collider
		var my_global_transform = get_global_transform()
		var player_global_transform = collided_body.get_global_transform()        
		collision_transform = player_global_transform.inverse() * my_global_transform  
				
		# make the arrow disappear after a second
		yield(get_tree().create_timer(1.0), "timeout")        
		queue_free()

func _apply_collision_transform():
	global_transform = collided_body.get_global_transform() * collision_transform

func _on_VisibilityNotifier2D_viewport_exited(_viewport):    
	queue_free()
