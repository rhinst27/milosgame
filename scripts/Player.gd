extends KinematicBody2D

export (int) var run_speed = 500
export (int) var jump_speed = -450
export (int) var gravity = 1200

var idle_timer = 0
var jumping = false
var ducking = false
var running = false
var getting_hurt = false
 
var velocity = Vector2()
var colliding_normal = null

var blood_spray = preload("res://scenes/BloodSpray.tscn") as PackedScene

func get_input():
	velocity.x = 0
	var right = Input.is_action_pressed('ui_right')
	var left = Input.is_action_pressed('ui_left')
	var jump = Input.is_action_just_pressed('ui_up')
	var duck = Input.is_action_pressed('ui_down')

	if jump and is_on_floor():
		jumping = true        
		velocity.y = jump_speed    
	if right and not duck:
		velocity.x += run_speed        
	elif left and not duck:
		velocity.x -= run_speed    
	
	running = (left or right) and not ducking
	ducking = duck and not jumping 


func _physics_process(delta):
	if getting_hurt:
		_process_hit(delta)
	else:
		get_input()
		_process_normal_movement(delta)
		
	
func _process_normal_movement(delta):    
	velocity.y += gravity * delta
	if jumping and is_on_floor():
		jumping = false
	
	_set_animation()
	_set_collision_shape()    
	velocity = move_and_slide(velocity, Vector2(0, -1))
	
	if velocity.length() > 0:
		idle_timer = 0
	else:
		idle_timer += delta

func _set_collision_shape():    
	if ducking:
		$StandingCollision.disabled = true
		$DuckingCollision.disabled = false        
	else:
		$StandingCollision.disabled = false
		$DuckingCollision.disabled = true       
	
func _set_animation():
	if not is_on_floor():
		if velocity.y < 0:
			$AnimatedSprite.animation = "jump"
		else:
			$AnimatedSprite.animation = "fall"   
	elif ducking:        
		$AnimatedSprite.animation = "duck"
		$AnimatedSprite.flip_h = false        
	elif running:         
		$AnimatedSprite.animation = "run"        
		$AnimatedSprite.flip_h = velocity.x < 0
	else:                
		if idle_timer > 5:
			$AnimatedSprite.animation = "think"    
		else:
			$AnimatedSprite.animation = "idle"    
				
			
func _process_hit(delta):        
	$AnimatedSprite.animation = "hit"    
	velocity = Vector2(-20000, -100)
	move_and_slide(velocity * delta, Vector2(0, -1))    
	
			
func get_hurt():
	getting_hurt = true
	$PainSound.play()
	var blood = blood_spray.instance()    
	add_child(blood)            
	yield(get_tree().create_timer(0.1), "timeout")    
	getting_hurt = false
	
