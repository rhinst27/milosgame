extends Panel

onready var player_vars = get_node("/root/GlobalSettings")

var game_scene = preload("res://scenes/Game.tscn") as PackedScene
var selected_item = 0
var menu_items = [
	["btn_start", "start_game"],
	["btn_mode", "toggle_mode"],
	["btn_quit", "quit"]
]

func _ready():               
	get_tree().paused = false
	redraw_selection_marker()
	redraw_mode()

func _input(event):    
	if(event.is_action_pressed("ui_down")):
		selected_item = ((selected_item + 1) % len(menu_items))
		redraw_selection_marker()
	elif(event.is_action_pressed("ui_up")):
		selected_item = ((selected_item - 1) % len(menu_items))
		redraw_selection_marker()
	elif(event.is_action_pressed("ui_accept") or event.is_action_pressed("ui_select")):
		call(menu_items[selected_item][1])

func redraw_selection_marker():
	var vbox: VBoxContainer = get_node("vbox_buttons")
	var active_button: Button = vbox.get_node(menu_items[selected_item][0])
	$SelectionMarker.position.y = vbox.rect_position.y + active_button.rect_position.y + 15
	$SelectionMarker.position.x = vbox.rect_position.x + active_button.rect_position.x - 20
	
func redraw_mode():
	var vbox: VBoxContainer = get_node("vbox_buttons")
	for item in menu_items:
		if item[1] == "toggle_mode":
			var btn: Button = vbox.get_node(item[0])
			btn.text = "MODE: " + player_vars.DIFFICULTY_SETTINGS[player_vars.difficulty_mode]["name"]

func start_game():
	print("Pressed start")
	get_tree().change_scene_to(game_scene)
	
func toggle_mode():
	print("Toggled Difficulty Mode")
	player_vars.difficulty_mode = ((player_vars.difficulty_mode + 1) % len(player_vars.DIFFICULTY_SETTINGS))
	redraw_mode()
	

func quit():
	get_tree().quit()
