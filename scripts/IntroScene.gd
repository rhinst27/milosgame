extends Node2D

const SCROLL_SPEED = 100

var menu_scene = preload("res://scenes/MainMenu.tscn") as PackedScene

func _physics_process(delta):
	if(Input.is_action_pressed("ui_cancel") or Input.is_action_pressed("ui_select")):
		skip_to_menu()
	$MovingTextBlock.move_and_collide(Vector2(0, -1 * SCROLL_SPEED * delta))

func skip_to_menu():
	get_tree().change_scene_to(menu_scene)

func _on_VisibilityNotifier2D_screen_exited():
	skip_to_menu()
